# 3D printer projects

various 3D printer STLs I've designed for all kinds of stuff

feel free to use, modify, build on these as much as you like.

BlackDecker battery base.stl :: Adapter to slide on to Black and Decker 12-18V battery packs for mounting custom devices on, like building your own power bank or worklight. This is a base concept with various extension modules already designed that I will upload soon.

Xmas Tree2.stl :: basic xmas tree frame to hold a full set of the common WS2812b programmable RGB LED rings (241 LEDs)

esp-18650-oled-box-v4.stl :: rectangular case for a common series of ESP8266 modules with 18650 battery holder, joystick and 0.96" OLED display that's sold on AliExpress under various brands

esp-18650-oled-deckel-v4.stl :: top cover for above ESP8266box case

esp32-oled-box-v1.stl :: rectangular case for a common series of ESP32 modules with 18650 battery holder and 0,96" OLED display, also available on AliExpress under various brands

esp32-oled-box-deckel.stl :: top cover for above ESP32 box case
